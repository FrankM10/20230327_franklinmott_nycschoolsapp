//
//  _0230327_FranklinMott_NYCSchoolsTests.swift
//  20230327-FranklinMott-NYCSchoolsTests
//
//  Created by Franklin Mott on 3/27/23.
//

import XCTest
@testable import _0230327_FranklinMott_NYCSchools

final class _0230327_FranklinMott_NYCSchoolsTests: XCTestCase {


    func testGetSchoolData() async {
        let vm1 = SchoolListViewModel(networkManager: NetworkManager())
        await vm1.getDataForSchoolList(urlString: APIEndPoints.schoolAPIEndpoint)
        XCTAssertNotNil(vm1.schoolList, "Expecting School List Data")
        
    }
    func testGetSATData() async {
        let vm1 = SchoolListViewModel(networkManager: NetworkManager())
        await vm1.getDataForSchoolList(urlString: APIEndPoints.schoolAPIEndpoint)
        let vm = SchoolDetailsViewModel(networkManager: NetworkManager())
        let sD = vm1.schoolList[1].dbn ?? ""
        await vm.getDataForSchoolDetails(urlString: "\(APIEndPoints.satAPIEndpoint)" + sD)
        XCTAssertNotNil(vm.schoolDetails, "Expecting School Details Data")
    }
    

}
