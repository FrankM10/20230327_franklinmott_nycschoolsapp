//
//  _0230327_FranklinMott_NYCSchoolsApp.swift
//  20230327-FranklinMott-NYCSchools
//
//  Created by Franklin Mott on 3/27/23.
//

import SwiftUI

@main
struct _0230327_FranklinMott_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: SchoolListViewModel(networkManager: NetworkManager()))
        }
    }
}
