//
//  ContentView.swift
//  20230327-FranklinMott-NYCSchools
//
//  Created by Franklin Mott on 3/27/23.
//

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel : SchoolListViewModel
    @State var isErrorOccured: Bool = false
    
    var body: some View {
        NavigationView{
            VStack{
                List{
                    ForEach($viewModel.schoolList){ school in
                        NavigationLink{
                            DetailView(viewModel: SchoolDetailsViewModel(networkManager: NetworkManager()), schoolData: school)
                        } label: {
                            ListCell(schoolData: school)
                        }
                    }
                }
                .edgesIgnoringSafeArea(.all)
    
            }
        }
        .onAppear{
            Task{
                await viewModel.getDataForSchoolList(urlString:APIEndPoints.schoolAPIEndpoint)
            }
        }
        .refreshable {
            await viewModel.getDataForSchoolList(urlString: APIEndPoints.schoolAPIEndpoint)
        }
    }
}
