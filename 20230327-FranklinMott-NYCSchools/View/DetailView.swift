//
//  DetailView.swift
//  20230327-FranklinMott-NYCSchools
//
//  Created by Franklin Mott on 3/27/23.
//

import SwiftUI

struct DetailView: View {
    @StateObject var viewModel : SchoolDetailsViewModel
    @Binding var schoolData: SchoolListData

    
    var body: some View {
        ZStack{
            LinearGradient(colors: [.red,.white,.blue], startPoint: .topTrailing, endPoint: .bottomTrailing)
            VStack{
                Text("Name: " + (schoolData.school_name ?? "") ).padding(40).font(.headline).multilineTextAlignment(.center)
                Text("Address: " + (schoolData.primary_address_line_1 ?? "") ).padding(40).font(.headline)
                Text("City: " + (schoolData.city ?? "") ).padding(40).font(.headline)
                Text("Sat Details").padding(20).font(.headline)
                
                Text("Critical Reading AvgScore: " + (viewModel.schoolDetails?.satCriticalReadingAvgScore ?? "Data N/A") ).font(.body).padding(5)
                Text("Math Avg Score: " + (viewModel.schoolDetails?.satMathAvgScore ?? "Data N/A") ).font(.body).padding(5)

                Text("Writing Avg Score: " + (viewModel.schoolDetails?.satWritingAvgScore ?? "Data N/A") ).font(.body)

            }
        }
        .edgesIgnoringSafeArea(.all)
        .onAppear{
            Task{
                let url = "\(APIEndPoints.satAPIEndpoint)\(schoolData.dbn)"
                await viewModel.getDataForSchoolDetails(urlString: url)

            }
        }
        
        .refreshable{
            Task{
                let url = "\(APIEndPoints.satAPIEndpoint)\(schoolData.dbn)"
                await viewModel.getDataForSchoolDetails(urlString: url)
            }
          
        }

    }
}
