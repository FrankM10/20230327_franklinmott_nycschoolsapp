//
//  ListCell.swift
//  20230327-FranklinMott-NYCSchools
//
//  Created by Franklin Mott on 3/27/23.
//

import SwiftUI

struct ListCell: View {
    @Binding var schoolData: SchoolListData
    
    var body: some View {
        ZStack {
            LinearGradient(colors: [.blue,.white,.red], startPoint: .leading, endPoint: .trailing)
            VStack{
                Text("Name: " + (schoolData.school_name ?? "") ).padding().font(.headline).multilineTextAlignment(.center)
                Text("Address: " + (schoolData.primary_address_line_1 ?? "") ).padding().font(.headline).multilineTextAlignment(.center)
                Text("City: " + (schoolData.city ?? "") ).padding().font(.body)
            }
            .cornerRadius(20)
        }
        .cornerRadius(20)
    }
}

struct ListCell_Previews: PreviewProvider {
    @State static var school = SchoolListData(dbn: "", school_name: "", boro: "", overview_paragraph: "", school_10th_seats: "", academicopportunities1: "", academicopportunities2: "", ell_programs: "", neighborhood: "", building_code: "", location: "", phone_number: "", fax_number: "", school_email: "", website: "", subway: "", grades2018: "", finalgrades: "", total_students: "", extracurricular_activities: "", school_sports: "", attendance_rate: "", requirement1_1: "", requirement2_1: "", requirement3_1: "", requirement4_1: "", requirement5_1: "", offer_rate1: "", program1: "", interest1: "", primary_address_line_1: "", city: "")
    static var previews: some View {
        ListCell(schoolData: $school )
    }

}

