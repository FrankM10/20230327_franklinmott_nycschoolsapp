//
//  NetworkManager.swift
//  20230327-FranklinMott-NYCSchools
//
//  Created by Franklin Mott on 3/27/23.
//

import Foundation

protocol Networkable{
    func getDataFromAPI(url: URL) async throws -> Data
}

class NetworkManager: Networkable{
    
    func getDataFromAPI(url: URL) async throws -> Data{
        do{
           let (data,_)  = try await URLSession.shared.data(from: url)
            return data
        }catch{
            throw NetworkError.dataNotFound
        }
    }
}

