//
//  APIEndPoints.swift
//  20230327-FranklinMott-NYCSchools
//
//  Created by Franklin Mott on 3/27/23.
//

import Foundation

struct APIEndPoints{
    static var schoolAPIEndpoint = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static var satAPIEndpoint = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="

}
