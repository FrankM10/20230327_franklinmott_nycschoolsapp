//
//  SchoolDetailsViewModel.swift
//  20230327-FranklinMott-NYCSchools
//
//  Created by Franklin Mott on 3/27/23.
//

import Foundation

class SchoolDetailsViewModel: ObservableObject {
    @Published var customError: NetworkError?
    @Published var schoolDetails: SchoolDetails?
    private let networkManager : Networkable

    init( networkManager: Networkable) {
        self.networkManager = networkManager
    }
    func getDataForSchoolDetails(urlString:String) async {
        guard let url =  URL(string:urlString) else {
            customError = NetworkError.invalidURL
            return
        }
        do{
            let schoolData = try await networkManager.getDataFromAPI(url: url)
           let schoolParsedData = try JSONDecoder().decode([SchoolDetails].self, from: schoolData)
            DispatchQueue.main.async {
                self.schoolDetails = schoolParsedData.first
            }
            print(schoolParsedData)
        }catch let someError{
            print(someError.localizedDescription)
            DispatchQueue.main.async {
                
                if someError as? NetworkError == .dataNotFound{
                    self.customError = NetworkError.dataNotFound
                }else{
                    self.customError = NetworkError.parsingError
                }
            }
        

        }
        
    }
}
