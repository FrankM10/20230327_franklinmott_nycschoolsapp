//
//  SchoolListViewModel.swift
//  20230327-FranklinMott-NYCSchools
//
//  Created by Franklin Mott on 3/27/23.
//

import Foundation

final class SchoolListViewModel: ObservableObject {
    @Published var schoolList: [SchoolListData] = []
    @Published var customError: NetworkError?
    private let networkManager : Networkable
    
    init( networkManager: Networkable) {
        self.networkManager = networkManager
    }
    
    func getDataForSchoolList(urlString:String) async {
        guard let url =  URL(string:urlString) else {
            customError = NetworkError.invalidURL
            return
        }
        do{
            let schoolData = try await networkManager.getDataFromAPI(url: url)
            let schoolParsedData = try JSONDecoder().decode([SchoolListData].self, from: schoolData)
            DispatchQueue.main.async {
                self.schoolList = schoolParsedData
            }
            
            print(schoolParsedData)
        }catch let someError{
            print(someError.localizedDescription)
            DispatchQueue.main.async {
                
                if someError as? NetworkError == .dataNotFound{
                    self.customError = NetworkError.dataNotFound
                }else{
                    self.customError = NetworkError.parsingError
                }
            }
            
        }
        
    }
}
